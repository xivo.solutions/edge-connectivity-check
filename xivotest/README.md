# TestRTC #
Xivo Edge check provides a set of tests that can be easily run by a user to help diagnose WebRTC related issues. 
The user can then download a report containing all the gathered information or upload the log and create a temporary link with the report result.

## Automatic tests ##
* Network
  * Udp/Tcp
    * Verifies it can talk with a turn server with the given protocol
  * IPv6 connectivity
    * Verifies it can gather at least one IPv6 candidate
* Connectivity
  * Relay
    * Verifies connections can be established between peers through a TURN server
  * Reflexive
    * Verifies connections can be established between peers through NAT
  * Host
    * Verifies connections can be established between peers with the same IP address
* Throughput
  * Data throughput
    * Establishes a loopback call and tests data channels throughput on the link
* Microphone
  * Audio capture
    * Checks the microphone is able to produce 2 seconds of non-silent audio
    * Computes peak level and maximum RMS
    * Clip detection
    * Mono mic detection
* Camera
  * Check WxH resolution
    * Checks the camera is able to capture at the requested resolution for 5 seconds
    * Checks if the frames are frozen or muted/black
    * Detects how long to start encode frames
    * Reports encode time and average framerate
  * Check supported resolutions
    * Lists resolutions that appear to be supported


## Development ##
Make sure to install NodeJS and NPM before continuing. Note that we have been mainly been using Posix when developing TestRTC hence developer tools might not work correctly on Windows.

#### Install developer tools and frameworks ####
```bash
npm install
```

#### Install dependencies ####
```bash
bower update
```

#### Run linters (currently very limited set is run) ####
```bash
grunt
```

#### Build testrtc ####
Cleans out/ folder if it exists else it's created, then it copies and vulcanizes the resources needed to deploy.
```
grunt build
```

