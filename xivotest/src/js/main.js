/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
'use strict';
/* exported addExplicitTest, addTest */

var enumeratedTestSuites = [];
var enumeratedTestFilters = [];

function addTest(suiteName, testName, func) {
  if (isTestDisabled(testName)) {
    return;
  }

  for (var i = 0; i !== enumeratedTestSuites.length; ++i) {
    if (enumeratedTestSuites[i].name === suiteName) {
      enumeratedTestSuites[i].addTest(testName, func);
      return;
    }
  }
  // Non-existent suite create and attach to #content.
  var suite = document.createElement('testrtc-suite');
  suite.name = suiteName;
  suite.addTest(testName, func);
  enumeratedTestSuites.push(suite);
  document.getElementById('content').appendChild(suite);
}

// Add a test that only runs if it is explicitly enabled with
// ?test_filter=<TEST NAME>
function addExplicitTest(suiteName, testName, func) {
  if (isTestExplicitlyEnabled(testName)) {
    addTest(suiteName, testName, func);
  }
}

function isTestDisabled(testName) {
  if (enumeratedTestFilters.length === 0) {
    return false;
  }
  return !isTestExplicitlyEnabled(testName);
}

function isTestExplicitlyEnabled(testName) {
  for (var i = 0; i !== enumeratedTestFilters.length; ++i) {
    if (enumeratedTestFilters[i] === testName) {
      return true;
    }
  }
  return false;
}

var parameters = parseUrlParameters();
var filterParameterName = 'test_filter';
if (filterParameterName in parameters) {
  enumeratedTestFilters = parameters[filterParameterName].split(',');
}

function downloadFile() {
  const itemHeader = document.getElementsByClassName("header style-scope testrtc-test");
  const data = document.createElement("ul");
  data.setAttribute("display", "none");
  var obj = [];
  for (let i = 0; i < itemHeader.length; i++) {
    try{
      obj.push({type: "title", text: itemHeader[i].children[0].firstChild.data});
      itemHeader[i].nextElementSibling.children.forEach(element => {
        if (element.innerText) {
          obj.push({type: "message", text: element.innerText});
        }
      });
    }
    catch(e){
      console.warn("Warn : item don't have message => ",e);
    }
  };
  for (let i = 0; i < obj.length; i+=1) {
    if (obj[i].type == 'title'){
      let li = document.createElement("li");
      li.innerText = obj[i].text;
      data.appendChild(li);
    }
    else if (obj[i].type == 'message') {
      let span = document.createElement("span");
      span.innerText = obj[i].text;
      data.appendChild(span);
    }
  }
  console.log("Print log : ",data);
  var opt = {
    margin:       1,
    filename:     'edge-report.pdf',
    image:        { type: 'jpeg', quality: 0.98 },
    html2canvas:  { scale: 2 },
    jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
  };
  html2pdf().set(opt).from(data).save();
}
