FROM nginx:stable-alpine-slim

WORKDIR /usr/share/nginx/html


COPY ./loginpage/dist /usr/share/nginx/html/login

RUN apk add npm
RUN apk add git
RUN npm install -g grunt
RUN npm install -g bower

COPY /xivotest /var/tmp/build

WORKDIR /var/tmp/build

RUN npm ci
RUN bower update
RUN grunt build

RUN mkdir /usr/share/nginx/html/test
RUN mv out/* /usr/share/nginx/html/test/

COPY default.conf /etc/nginx/conf.d/

WORKDIR /usr/share/nginx/html

RUN rm -rf /var/tmp/build

EXPOSE 80
EXPOSE 443
