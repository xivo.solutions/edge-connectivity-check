## Project objective

This project is a test page to check if the edge have a good configuration.
For this you need to have one Cti user to login and the xuc IP.

## Launch project

To launch the project you have to copy the following bash command at the root of the project


```bash
cd loginpage
npm ci
npm run build
cd ../xivotest
npm ci
bower update
grunt build
```

Now you have to build and docker images regardless

For the login page :

```docker
docker build -t edge-connectivity-check_loginpage .
docker run -p 8080:80 edge-connectivity-check_loginpage:latest
```

For the test page :
```docker
docker build -t edge-connectivity-check_xivotest .
docker run -p 8888:80 edge-connectivity-check_xivotest:latest
```